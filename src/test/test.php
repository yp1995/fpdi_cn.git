<?php


namespace setasignCn\Fpdi\test;


class test
{
    public function word()
    {
        // New class,load template
        $TemplateProcessor = new WordProcessor();
        $template = 'temple.docx';
        $TemplateProcessor->load($template);

// Set Value
        $TemplateProcessor->setValue('value', 'r-value');

// Clone
        $TemplateProcessor->clones('people', 3);

        $TemplateProcessor->setValue('name#0', 'colin0');
        $TemplateProcessor->setValue('name#1', [
            ['text'=>'colin1','style'=>'style','type'=>MDWORD_TEXT],
            ['text'=>1,'type'=>MDWORD_BREAK],
            ['text'=>'86','style'=>'style','type'=>MDWORD_TEXT]
        ]);
        $TemplateProcessor->setValue('name#2', 'colin2');

        $TemplateProcessor->setValue('sex#1', 'woman');

        $TemplateProcessor->setValue('age#0', '280');
        $TemplateProcessor->setValue('age#1', '281');
        $TemplateProcessor->setValue('age#2', '282');

// set value for image
        $TemplateProcessor->setImageValue('image', dirname(__FILE__).'/logo.jpg');

// Delete a paragraph
        $TemplateProcessor->deleteP('style');

// Save
        $rtemplate = __DIR__.'/r-temple.docx';
        $TemplateProcessor->saveAs($rtemplate);
    }
}