# composer require setasign-cn/fpdi-cn

## 目录
使用方法
###
###composer require setasign-cn/fpdi-cn
###
```php


$pdf = new \setasignCn\Fpdi\PdfWatermark();
// type 是1 是 $text 水印文字  0.6 是水印的透明度（0-1）
//   $pdf->SetWatermarkText($text,0.6);
// type 2 事 图片水印地址 
$pdf->setImage('./sy1.png');
// addWatermark 参数说明  
// type 1是文字 2 是图片水印 
// $path pdf 路径
//  $path_new  加水印之后的地址
$pdf->addWatermark($type,$path,$path_new);
```
#新增 word模版加数据
使用案例在=>  src/test/samples 
```php
// New class,load template
        $TemplateProcessor = new WordProcessor();
        $template = 'temple.docx';
        $TemplateProcessor->load($template);

// Set Value
        $TemplateProcessor->setValue('value', 'r-value');

// Clone
        $TemplateProcessor->clones('people', 3);

        $TemplateProcessor->setValue('name#0', 'colin0');
       
        $TemplateProcessor->setValue('name#2', 'colin2');

        $TemplateProcessor->setValue('sex#1', 'woman');

        $TemplateProcessor->setValue('age#0', '280');
        $TemplateProcessor->setValue('age#1', '281');
        $TemplateProcessor->setValue('age#2', '282');

// set value for image
        $TemplateProcessor->setImageValue('image', dirname(__FILE__).'/logo.jpg');

// Delete a paragraph
        $TemplateProcessor->deleteP('style');

// Save
        $rtemplate = __DIR__.'/r-temple.docx';
        $TemplateProcessor->saveAs($rtemplate);
